# Anilist Exporter

Small program to export the anime and manga lists of an Anilist account to
MyAnimeList's format.

*This project isn't in a functioning state yet. It's only here so that people
 can follow the development. You can still compile and run at your own risk 
 though.*

## License

This project is licensed under the WTFPL (full text in LICENSE).

Copyright (c) 2016 Mériadec d'Armorique