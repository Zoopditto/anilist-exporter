package exporter.MAL

import exporter.anilist.MediaInfo
import exporter.anilist.animeTypes.{AnimeInfo, AnimeRecord}
import exporter.anilist.mangaTypes.{MangaInfo, MangaRecord}

/**
  * Created by Zoopditto on 25/04/2016.
  */
object InfoUtils {

  def approximateTitleMatch(target: String)(candidate: MediaInfo): Boolean = {
    val title = candidate.titleRomaji
    title.toLowerCase == target.toLowerCase
  }

  def animeTypeMatch(target: String)(candidate: AnimeInfo): Boolean = {
    val mediaType = candidate.mediaType
    mediaType.toLowerCase == target.toLowerCase
  }

  def isManga()(mangaInfo: MangaInfo): Boolean =
    mangaInfo.mediaType.toLowerCase == "manga"

  /**
    * Tries to narrow down the amount of potential entries for an anime title
    *
    * @param anime
    * @param options
    * @return Either a set of (hopefully) narrowed down options, or a single
    *         entry
    */
  def reduceAnimeAmbiguity(anime: AnimeRecord, options: Set[AnimeInfo]):
  Either[Set[AnimeInfo], AnimeInfo] = {
    var filtered: Set[AnimeInfo] = options
    Seq(animeTypeMatch(anime.info.mediaType)(_),
      approximateTitleMatch(anime.info.titleRomaji)(_))
      .foreach { pred =>
      val filteredTmp = filtered.filter(pred)
        filteredTmp.size match {
          case 1 => return Right(filteredTmp.head)
          case 0 => ()
          case _ => filtered = filteredTmp
        }
      }
    Left(filtered)
  }

  /**
    * Tries to narrow down the amount of potential entries for a manga title
    *
    * @param manga
    * @param options
    * @return Either a set of (hopefully) narrowed down options, or a single
    *         entry
    */
  def reduceMangaAmbiguity(manga: MangaRecord, options: Set[MangaInfo]):
  Either[Set[MangaInfo], MangaInfo] = {
    var filtered: Set[MangaInfo] = options
    Seq(isManga()(_),
      approximateTitleMatch(manga.info.titleRomaji)(_))
      .foreach { pred =>
        val filteredTmp = filtered.filter(pred)
        filteredTmp.size match {
          case 1 => return Right(filteredTmp.head)
          case 0 => ()
          case _ => filtered = filteredTmp
        }
    }
    Left(filtered)
  }
}
