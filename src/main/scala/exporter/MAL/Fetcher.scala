package exporter.MAL

import java.net.URLEncoder

import akka.actor.{ActorRef, FSM}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.BasicHttpCredentials
import akka.pattern.pipe
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import exporter.MAL.PoolSupervisor.Response
import exporter.anilist.{MediaInfo, MediaRecord}
import exporter.anilist.animeTypes.{AnimeInfo, AnimeRecord}
import exporter.anilist.mangaTypes.{MangaInfo, MangaRecord}
import exporter.core.Settings

import scala.language.postfixOps
import scala.xml.{Elem, Node, NodeSeq, XML}

/**
  * Created by Zoopditto on 23/04/2016.
  */
object Fetcher {
  case class FetchMedia(media: MediaRecord)
  sealed trait Response
  case class Success(media: MediaRecord) extends Response
  case class AnimeAmbiguity(media: AnimeRecord, options: Set[AnimeInfo]) extends Response
  case class MangaAmbiguity(media: MangaRecord, options: Set[MangaInfo]) extends Response
  case class Failure(media: MediaRecord) extends Response

  sealed trait State
  case object Inactive extends State
  case object PendingRequest extends State
  case object PendingConversion extends State

  case class Data(redirect: Option[ActorRef],
                  media: Option[MediaRecord])

  def formatStringToHttp(s: String): String = {
    URLEncoder.encode(s.toLowerCase, "UTF-8")
  }

  def nodeToAnimeInfo(node: Node): AnimeInfo =
    AnimeInfo(
      id = (node \ "id").text.toInt,
      titleRomaji = (node \ "title").text,
      mediaType = (node \ "type").text,
      adult = false,
      releaseStatus = (node \ "status").text,
      totalEpisodes = Some((node \ "episodes").text.toInt))

  def nodeToMangaInfo(node: Node): MangaInfo =
    MangaInfo(
      id = (node \ "id").text.toInt,
      titleRomaji = (node \ "title").text,
      mediaType = (node \ "type").text,
      adult = false,
      releaseStatus = (node \ "status").text,
      totalChapters = Some((node \ "chapters").text.toInt),
      totalVolumes = Some((node \ "volumes").text.toInt))
}
class Fetcher extends FSM[Fetcher.State, Fetcher.Data] {
  import Fetcher._
  import context.dispatcher

  startWith(Inactive, Data(None, None))

  when(Inactive) {
    case Event(FetchMedia(anime: AnimeRecord), _) =>
      searchRequest(Settings.malApiAnimeSearchURL, anime.info.titleRomaji)
      goto(PendingRequest) using Data(Some(sender), Some(anime))
    case Event(FetchMedia(manga: MangaRecord), _) =>
      searchRequest(Settings.malApiMangaSearchURL, manga.info.titleRomaji)
      goto(PendingRequest) using Data(Some(sender), Some(manga))
  }

  when(PendingRequest) {
    case Event(HttpResponse(StatusCodes.OK, _, entity, _), Data(_, Some(_))) =>
      entity.dataBytes.runReduce(_ ++ _)
        .map(_.decodeString("US-ASCII"))
        .map(XML.loadString)
        .pipeTo(self)
      goto(PendingConversion)
    case Event(HttpResponse(status, _, _, _), Data(Some(snd), Some(m))) =>
      snd ! Response(Fetcher.Failure(m))
      goto(Inactive) using Data(None, None)
  }

  when(PendingConversion) {
    case Event(payload: Elem, Data(Some(snd), Some(m))) =>
      val correctedMedia = correctMediaInfo(m, payload)
      snd ! Response(correctedMedia)
      goto(Inactive) using Data(None, None)
  }

  initialize

  final implicit val materializer =
    ActorMaterializer(ActorMaterializerSettings(context.system))
  val http = Http(context.system)

  def searchRequest(baseURL: String, term: String): Unit = {
    val url = s"$baseURL?q=${formatStringToHttp(term)}"
    val credentials = BasicHttpCredentials(
      Settings.malUsername, Settings.malPassword)
    http.singleRequest(HttpRequest(
      uri = url,
      headers = List(headers.Authorization(credentials))))
      .pipeTo(self)
  }

  def correctMediaInfo(mediaRecord: MediaRecord, xml: Elem): Fetcher.Response = {
    val entries = xml \\ "entry"
    mediaRecord match {

      case anime: AnimeRecord =>
        if (entries.length == 1) {
          val info = nodeToAnimeInfo(entries.head)
          Fetcher.Success(anime.copy(info = info))
        } else {
          val infos = entries.map(nodeToAnimeInfo)
          InfoUtils.reduceAnimeAmbiguity(anime, infos.toSet) match {
            case Right(info) => Fetcher.Success(anime.copy(info = info))
            case Left(options) => Fetcher.AnimeAmbiguity(anime, options)
          }
        }

      case manga: MangaRecord =>
        if (entries.length == 1) {
          val info = nodeToMangaInfo(entries.head)
          Fetcher.Success(manga.copy(info = info))
        } else {
          val infos = entries.map(nodeToMangaInfo)
          InfoUtils.reduceMangaAmbiguity(manga, infos.toSet) match {
            case Right(info) => Fetcher.Success(manga.copy(info = info))
            case Left(options) => Fetcher.MangaAmbiguity(manga, options)
          }
        }
    }
  }
}
