package exporter.MAL

import java.io.FileWriter

import exporter.MAL.FetcherSupervisor.ValidEntry
import exporter.anilist.MediaList
import exporter.anilist.animeTypes.{AnimeList, AnimeRecord}
import exporter.anilist.mangaTypes.{MangaList, MangaRecord}

import scala.collection.immutable.Queue
import scala.xml.dtd.DocType
import scala.xml.{Elem, Node, Unparsed, XML}

/**
  * Created by Zoopditto on 26/04/2016.
  */
object FileFormatting {

  val baseXml: Elem =
    <myanimelist></myanimelist>

  val anilistToMALAnimeStatus: Map[String, String] = Map(
    "completed" -> "Completed",
    "watching" -> "Watching",
    "dropped" -> "Dropped",
    "plan to watch" -> "Plan to Watch",
    "on-hold" -> "On-Hold"
  )

  val anilistToMALMangaStatus: Map[String, String] = Map(
    "completed" -> "Completed",
    "reading" -> "Reading",
    "dropped" -> "Dropped",
    "plan to read" -> "Plan to Read",
    "on-hold" -> "On-Hold"
  )

  def renderAnimeFile(list: AnimeList, queue: Queue[ValidEntry]): Unit = {
    val xml = appendChild(baseXml, animeUserInfo(list))
    val fullXml = queue.foldLeft(xml) { (xml, entry) =>
      appendChild(xml, animeEntry(entry.media.asInstanceOf[AnimeRecord]))
    }
    val writer = new FileWriter(s"animelist_${list.displayName}.xml")
    XML.write(w = writer,
      node = fullXml,
      enc = "UTF-8",
      xmlDecl = true,
      doctype = null)
    writer.close()
  }

  def renderMangaFile(list: MangaList, queue: Queue[ValidEntry]): Unit = {
    val xml = appendChild(baseXml, mangaUserInfo(list))
    val fullXml = queue.foldLeft(xml) { (xml, entry) =>
      appendChild(xml, mangaEntry(entry.media.asInstanceOf[MangaRecord]))
    }
    val writer = new FileWriter(s"mangalist_${list.displayName}.xml")
    XML.write(w = writer,
      node = fullXml,
      enc = "UTF-8",
      xmlDecl = true,
      doctype = null)
    writer.close()
  }

  def animeUserInfo(list: AnimeList): Elem =
    <myinfo>
      <user_id>0</user_id>
      <user_name>{ list.displayName }</user_name>
      <user_export_type>1</user_export_type>
      <user_total_anime>{ list.totalCount }</user_total_anime>
      <user_total_watching>{ list.watchingCount }</user_total_watching>
      <user_total_completed>{ list.completedCount }</user_total_completed>
      <user_total_onhold>{ list.onHoldCount }</user_total_onhold>
      <user_total_dropped>{ list.droppedCount }</user_total_dropped>
      <user_total_plantowatch>{ list.planCount }</user_total_plantowatch>
    </myinfo>

  def animeEntry(anime: AnimeRecord): Elem = {
    val totalEps: Int = anime.info.totalEpisodes.getOrElse(0)
    val watchedEps: Int = anime.episodesWatched.getOrElse(0)
    val notes: String = anime.notes.getOrElse("")
    val rewatched: Int = anime.rewatched.getOrElse(0)

    <anime>
      <series_animebd_id>{ anime.info.id }</series_animebd_id>
      <series_title>{ cdata(anime.info.titleRomaji) }</series_title>
      <series_type>{ anime.info.mediaType }</series_type>
      <series_episodes>{ totalEps }</series_episodes>
      <my_id>0</my_id>
      <my_watched_episodes>{ watchedEps }</my_watched_episodes>
      <my_start_date>0000-00-00</my_start_date>
      <my_finish_date>0000-00-00</my_finish_date>
      <my_rated></my_rated>
      <my_score>{ anime.scoreRaw / 10 }</my_score>
      <my_dvd></my_dvd>
      <my_storage></my_storage>
      <my_status>{ anilistToMALAnimeStatus(anime.listStatus) }</my_status>
      <my_comments>{ cdata(notes) }</my_comments>
      <my_times_watched>{ rewatched }</my_times_watched>
      <my_rewatch_value></my_rewatch_value>
      <my_tags>{ cdata("") }</my_tags>
      <my_rewatching>0</my_rewatching>
      <my_rewatching_ep>0</my_rewatching_ep>
      <update_on_import>1</update_on_import>
    </anime>
  }

  def mangaUserInfo(list: MangaList): Elem =
    <myinfo>
      <user_id>0</user_id>
      <user_name>{ list.displayName }</user_name>
      <user_export_type>2</user_export_type>
      <user_total_manga>{ list.totalCount }</user_total_manga>
      <user_total_reading>{ list.readingCount }</user_total_reading>
      <user_total_completed>{ list.completedCount }</user_total_completed>
      <user_total_onhold>{ list.onHoldCount }</user_total_onhold>
      <user_total_dropped>{ list.droppedCount }</user_total_dropped>
      <user_total_plantoread>{ list.planCount }</user_total_plantoread>
    </myinfo>

  def mangaEntry(manga: MangaRecord): Elem = {
    val totalVolumes: Int = manga.info.totalVolumes.getOrElse(0)
    val totalChapters: Int = manga.info.totalChapters.getOrElse(0)
    val volumesRead: Int = manga.volumesRead.getOrElse(0)
    val chaptersRead: Int = manga.chaptersRead.getOrElse(0)
    val notes: String = manga.notes.getOrElse("")
    val reread: Int = manga.reread.getOrElse(0)

    <manga>
      <manga_mangadb_id>{ manga.info.id }</manga_mangadb_id>
      <manga_title>{ cdata(manga.info.titleRomaji) }</manga_title>
      <manga_volumes>{ totalVolumes }</manga_volumes>
      <manga_chapters>{ totalChapters }</manga_chapters>
      <my_id>0</my_id> // I have no idea what this does
      <my_read_volumes>{ volumesRead }</my_read_volumes>
      <my_read_chapters>{ chaptersRead }</my_read_chapters>
      <my_start_date>0000-00-00</my_start_date>
      <my_finish_date>0000-00-00</my_finish_date>
      <my_scanlation_group>{ cdata("") }</my_scanlation_group>
      <my_score>{ manga.scoreRaw / 10 }</my_score>
      <my_status>{ anilistToMALMangaStatus(manga.listStatus) }</my_status>
      <my_comments>{ cdata(manga.notes) }</my_comments>
      <my_tags>{ cdata("") }</my_tags>
      <my_reread_value></my_reread_value>
      <update_on_import>1</update_on_import>
    </manga>
  }

  private def appendChild(n: Elem, newChild: Node*): Elem =
    n.copy(child = n.child ++ newChild)

  private def cdata(comment: Any) = Unparsed(s"<![CDATA[$comment]]>")
}
