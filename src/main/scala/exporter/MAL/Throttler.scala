package exporter.MAL

import java.util.concurrent.TimeUnit

import akka.AkkaException
import akka.actor.{Actor, ActorRef}

import scala.concurrent.duration.{Duration, TimeUnit}
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * Taken from the blog post by Roland Kuhn on letitcrash.com
  * Blog post: http://letitcrash.com/post/28901663062/throttling-messages-in-akka-2
  * Source: https://github.com/hbf/akka-throttler/blob/master/src/main/scala/akka/pattern/throttle/Throttler.scala
  */
trait Throttler {
  self: Actor =>
}

object Throttler {
  class FailedToSendException(message: String, cause: Throwable) extends AkkaException(message, cause)

  case class Rate(numberOfCalls: Int, duration: FiniteDuration) {
    def durationInMillis: Long = duration.toMillis
  }

  case class SetTarget(target: Option[ActorRef])
  case class SetRate(rate: Rate)
  case class Queue(msg: Any)

  case class RateInt(numberOfCalls: Int) {
    def msgsPer(duration: Int, timeUnit: TimeUnit) = Rate(numberOfCalls, Duration(duration, timeUnit))
    def msgsPer(duration: FiniteDuration) = Rate(numberOfCalls, duration)
    def msgsPerSecond = Rate(numberOfCalls, 1 second)
    def msgsPerMinute = Rate(numberOfCalls, 1 minute)
    def msgsPerHour = Rate(numberOfCalls, 1 hour)
  }

  implicit def toRateInt(numberOfCalls: Int): RateInt = RateInt(numberOfCalls)
}
