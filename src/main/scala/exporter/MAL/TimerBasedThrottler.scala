package exporter.MAL

import akka.actor.{Actor, ActorRef, LoggingFSM}
import exporter.MAL.Throttler.{FailedToSendException, Queue, Rate, SetRate, SetTarget}
import exporter.MAL.TimerBasedThrottler.{Data, State}

import scala.collection.immutable.{Queue => Q}
import scala.util.control.NonFatal

/**
  * Taken from the blog post by Roland Kuhn on letitcrash.com
  * Blog post: http://letitcrash.com/post/28901663062/throttling-messages-in-akka-2
  * Source: https://github.com/hbf/akka-throttler/blob/master/src/main/scala/akka/pattern/throttle/TimerBasedThrottler.scala
  */
object TimerBasedThrottler {
  case object Tick

  sealed trait State
  case object Idle extends State
  case object Active extends State

  case class Message(message: Any, sender: ActorRef)

  sealed case class Data(target: Option[ActorRef],
                         callsLeft: Int,
                         queue: Q[Message])
}
class TimerBasedThrottler(var rate: Rate)
  extends Actor with Throttler with LoggingFSM[State, Data] {
  import TimerBasedThrottler._

  startWith(Idle, Data(None, rate.numberOfCalls, Q[Message]()))

  when(Idle) {
    case Event(SetRate(r), d) =>
      this.rate = r
      stay using d.copy(callsLeft = r.numberOfCalls)

    case Event(SetTarget(t @ Some(_)), d) if d.queue.nonEmpty =>
      goto(Active) using deliverMessages(d.copy(target = t))
    case Event(SetTarget(t), d) =>
      stay using d.copy(target = t)

    case Event(Queue(msg), d @ Data(None, _, queue)) =>
      stay using d.copy(queue = queue.enqueue(Message(msg, context.sender)))
    case Event(Queue(msg), d @ Data(Some(_), _, Seq())) =>
      goto(Active) using deliverMessages(d.copy(
        queue = Q(Message(msg, context.sender))))
  }

  when(Active) {
    case Event(SetRate(r), d) =>
      this.rate = r
      stopTimer()
      startTimer(r)
      stay using d.copy(callsLeft = r.numberOfCalls)

    case Event(SetTarget(None), d) =>
      goto(Idle) using d.copy(target = None)
    case Event(SetTarget(t @ Some(_)), d) =>
      stay using d.copy(target = t)

    case Event(Queue(msg), d @ Data(_, 0, queue)) =>
      stay using d.copy(queue = queue.enqueue(Message(msg, context.sender)))
    case Event(Queue(msg), d @ Data(_, _, queue)) =>
      stay using deliverMessages(d.copy(
        queue = queue.enqueue(Message(msg, context.sender))))

    case Event(Tick, d @ Data(_, _, Seq())) =>
      goto(Idle)
    case Event(Tick, d @ Data(_, _, _)) =>
      stay using deliverMessages(d.copy(callsLeft = rate.numberOfCalls))
  }

  onTransition {
    case Idle -> Active => startTimer(rate)
    case Active -> Idle => stopTimer()
  }

  initialize

  private def deliverMessages(d: Data): Data = {
    import scala.math.min
    val queue = d.queue
    val nrOfMsgToSend = min(queue.length, d.callsLeft)
    queue.take(nrOfMsgToSend).foreach((x: Message) => {
      try {
        d.target.get.tell(x.message, x.sender)
      } catch {
        case NonFatal(e) => throw new FailedToSendException("tell() failed", e)
      }
    })

    d.copy(queue = d.queue.drop(nrOfMsgToSend),
      callsLeft = d.callsLeft - nrOfMsgToSend)
  }

  def startTimer(r: Rate) =
    setTimer("morePermits", Tick, rate.duration, repeat = true)
  def stopTimer() = cancelTimer("morePermits")
}
