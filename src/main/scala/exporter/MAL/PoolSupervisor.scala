package exporter.MAL

import akka.actor.{Actor, ActorRef, Props}
import exporter.MAL.PoolSupervisor.EmptyPool
import exporter.anilist.Media

/**
  * Created by Zoopditto on 24/04/2016.
  */
object PoolSupervisor {
  case class Launch(message: Any)
  case class Response(message: Any)
  case class EmptyPool(message: Any)
}
class PoolSupervisor(poolSize: Int, redirect: ActorRef)
  extends Actor {
  import PoolSupervisor.{Launch, Response}
  import context._

  var availablePool: Set[ActorRef] = Set()

  override def preStart: Unit = {
    (1 to poolSize).foreach { index: Int =>
      availablePool += actorOf(Props[Fetcher], s"worker_$index") // TODO:
      // find a way to make the class of actor variable
    }
  }

  def receive = {
    case Launch(message) =>
      if (availablePool.nonEmpty) {
        val worker = availablePool.head
        availablePool = availablePool.drop(1)
        worker ! message
      } else {
        sender ! EmptyPool(message)
      }

    case Response(message) =>
      redirect.tell(message, sender)
      availablePool += sender
  }
}
