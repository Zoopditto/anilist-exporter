package exporter.MAL

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import exporter.MAL.Throttler.{Queue, Rate, SetTarget}
import exporter.anilist.{MediaInfo, MediaList, MediaRecord}
import exporter.core.Settings

import scala.collection.immutable.{Queue => Q}
import scala.concurrent.duration._

/**
  * Created by Zoopditto on 23/04/2016.
  */
object FetcherSupervisor {
  sealed trait Message
  case class Start(home: ActorRef) extends Message
  case class Done(results: Q[Entry]) extends Message

  sealed trait Entry {
    val media: MediaRecord
    def isValid: Boolean = this match {
      case _: ValidEntry => true
      case _ => false
    }
    def isInvalid: Boolean = this match {
      case _: InvalidEntry => true
      case _ => false
    }
    def isAmbiguous: Boolean = this match {
      case _: AmbiguousEntry => true
      case _ => false
    }
  }
  case class ValidEntry(media: MediaRecord) extends Entry
  case class InvalidEntry(media: MediaRecord) extends Entry
  case class AmbiguousEntry(media: MediaRecord, options: Set[MediaInfo])
    extends Entry
}
class FetcherSupervisor(mediaList: MediaList, verbose: Boolean)
  extends Actor with ActorLogging {

  import FetcherSupervisor._
  import context._

  import scala.collection.mutable

  var entriesResult: mutable.Queue[Entry] = mutable.Queue()
  var home: Option[ActorRef] = None

  val pool = actorOf(Props(classOf[PoolSupervisor],
    Settings.fetcherPoolSize, self), "fetcherPool")
  val throttler = actorOf(Props(classOf[TimerBasedThrottler],
    Rate(Settings.fetcherThrottlingCalls,
      Settings.fetcherThrottlingLapseMillis.millis)))

  override def preStart: Unit = {
    throttler ! SetTarget(Some(pool))
  }

  def receive = {
    case Start =>
      home = Some(sender)
      launchWorkers()

    case PoolSupervisor.EmptyPool(message) => throttler ! Queue(message)

    // Fetcher responses

    case Fetcher.Success(media) =>
      if (verbose)
        log.info(s"Found entry for ${media.info.titleRomaji} " +
          s"($entriesProgress)")
      updateQueue(ValidEntry(media))
      printProgress()

    case Fetcher.Failure(media) =>
      if (verbose)
        log.info(s"Couldn't find entry for ${media.info.titleRomaji} " +
          s"($entriesProgress)")
      else printProgress()
      updateQueue(InvalidEntry(media))
      printProgress()

    case Fetcher.AnimeAmbiguity(anime, options) =>
      if (verbose)
        log.info(s"Several possible entries for ${anime.info.titleRomaji} " +
          s"($entriesProgress)")
      else printProgress()
      updateQueue(AmbiguousEntry(anime.asInstanceOf[MediaRecord],
        options.asInstanceOf[Set[MediaInfo]]))

    case Fetcher.MangaAmbiguity(manga, options) =>
      if (verbose)
        log.info(s"Several possible entries for ${manga.info.titleRomaji} " +
          s"($entriesProgress)")
      else printProgress()
      updateQueue(AmbiguousEntry(manga.asInstanceOf[MediaRecord],
        options.asInstanceOf[Set[MediaInfo]]))
  }

  def launchWorkers(): Unit = {
    mediaList.fullList.foreach { media: MediaRecord =>
      throttler ! Queue(PoolSupervisor.Launch(Fetcher.FetchMedia(media)))
    }
  }

  def updateQueue(entry: Entry) = {
    entriesResult.enqueue(entry)
    if (entriesResult.size == mediaList.fullList.length && home.isDefined)
      home.get ! Done(Q(entriesResult: _*)
        .sortWith(_.media.info.titleRomaji < _.media.info.titleRomaji))
  }

  def printProgress(): Unit = {
    val progressBarSize = 50
    val ratio = ((entriesResult.size.toFloat / mediaList.totalCount)
      * progressBarSize).toInt
    val filledBar =
      if (ratio > 0)
        (1 to ratio).map{_ => "="}.reduce(_++_)
      else ""
    val emptyBar =
      if (ratio < progressBarSize)
        (1 to (progressBarSize - ratio)).map{_ => " "}.reduce(_++_)
      else ""
    print(s"\r[$filledBar$emptyBar] ($entriesProgress)")
  }

  def entriesProgress: String = s"${entriesResult.size}/${mediaList.totalCount}"

}
