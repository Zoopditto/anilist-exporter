package exporter.core

import akka.actor.{ActorSystem, Props}
import akka.pattern.ask

import scala.concurrent.duration._
import scala.collection.immutable.{Queue => Q}
import scala.util.{Failure, Success, Try}
import scala.language.postfixOps
import exporter.MAL.{FetcherSupervisor, FileFormatting}
import exporter.MAL.FetcherSupervisor.{AmbiguousEntry, Entry, ValidEntry}
import exporter.anilist.animeTypes.AnimeList
import exporter.anilist.mangaTypes.MangaList
import exporter.anilist.{Anilist, Anime, Manga, MediaList}
import scopt.OptionParser

import scala.concurrent.{Future, TimeoutException}

/**
  * Created by Zoopditto on 20/04/2016.
  */
sealed trait Mode
case object AnimeFetching extends Mode
case object MangaFetching extends Mode

case class Configuration(username: String = "",
                         mode: Mode = AnimeFetching,
                         output: String = "list.xml",
                         input: String = "",
                         timeout: FiniteDuration = 300 second,
                         configFile: String = Settings.defaultFilename,
                         verbose: Boolean = false)
object Core {

  private val parser = new OptionParser[Configuration]("anilist-extractor") {
    head("anilist-extractor", "0.0.1")
    cmd("anime") action { (x, c) =>
      c.copy(mode = AnimeFetching)
    } text "Extracts <username>'s anime list from anilist.co"
    cmd("manga") action { (x, c) =>
      c.copy(mode = MangaFetching)
    } text "Extracts <username>'s manga list from anilist.co"
    help("help") text "Prints this usage text"
    arg[String]("<username>") action { (x, c) =>
      c.copy(username = x)
    } text "Username to get the list from"
    opt[String]('o', "output") action { (x, c) =>
      c.copy(output = x)
    } text "Output file"
    opt[String]('i', "input") action { (x, c) =>
      c.copy(input = x)
    } text "Input file (in case of export via json file)"
    opt[Int]('t', "timeout") action { (x, c) =>
      c.copy(timeout = x second)
    } text "Total maximum timeout for the MAL queries in seconds"
    opt[Unit]('v', "verbose") action { (_, c) =>
      c.copy(verbose = true)
    } text "Will print info on the media info fetching"
  }

  def checkConfigFile: Boolean = {
    import Settings._

    checkValidity match {
      case Some(NoFile) =>
        println(s"File missing or wrongly formatted: ${Settings.defaultFilename}")
        false
      case Some(InvalidParameters(params)) =>
        println(s"Missing or invalid parameters from ${Settings.defaultFilename}:")
        params.foreach(println)
        false
      case None =>
        true
    }
  }

  def startAnimeFetching(animeList: AnimeList, conf: Configuration): Unit = {
    import scala.concurrent.ExecutionContext.Implicits.global

    val system = ActorSystem("system")
    val supervisor = system.actorOf(Props(classOf[FetcherSupervisor],
      animeList, conf.verbose), "supervisor")

    println("Fetching anime entries from MAL's database...")
    val future = (supervisor ? FetcherSupervisor.Start)(conf.timeout).map {
      case FetcherSupervisor.Done(queue) =>
        Future(handleResults(animeList, queue)).map { _ =>
          system.terminate
        }
    }
    future.onFailure {
      case _: TimeoutException => "MAL queries timed out"
    }
  }

  def startMangaFetching(mangaList: MangaList, conf: Configuration): Unit = {
    import scala.concurrent.ExecutionContext.Implicits.global

    val system = ActorSystem("system")
    val supervisor = system.actorOf(Props(classOf[FetcherSupervisor],
      mangaList, conf.verbose), "supervisor")

    println("Fetching manga entries from MAL's database")
    val future = (supervisor ? FetcherSupervisor.Start)(conf.timeout).map {
      case FetcherSupervisor.Done(queue) =>
        Future(handleResults(mangaList, queue)).map { _ =>
          system.terminate
        }
    }
    future.onFailure {
      case _: TimeoutException => "MAL queries timed out"
    }
  }

  def handleResults(list: MediaList, queue: Q[Entry]): Unit = {
    println
    CliUtils.printResults(queue)
    val validEntries = queue.filter(_.isValid).asInstanceOf[Q[ValidEntry]]
    val fixedEntries = CliUtils.eliminateAmbiguity(queue.filter(_.isAmbiguous)
      .asInstanceOf[Q[AmbiguousEntry]])
    println("Rendering XML file...")
    list match {
      case animeList: AnimeList =>
        FileFormatting.renderAnimeFile(animeList, (validEntries ++ fixedEntries)
          .sortWith(_.media.info.titleRomaji < _.media.info.titleRomaji))
      case mangaList: MangaList =>
        FileFormatting.renderMangaFile(mangaList, (validEntries ++ fixedEntries)
          .sortWith(_.media.info.titleRomaji < _.media.info.titleRomaji))
    }
    println("Done")
  }

  def main(args: Array[String]): Unit = {
    parser.parse(args, Configuration()) match {
      case Some(conf) =>
        if (checkConfigFile) {
          conf.mode match {

            case AnimeFetching =>
               conf.input match {
                 case "" =>
                   println(s"Fetching ${conf.username}'s anime list from anilist...")
                   Thread.sleep(1000)
                   println("Actually this isn't doing anything since I haven't implemented this functionality yet.")
                   println("Sorry")
                 case _ =>
                   println(s"Parsing ${conf.input}...")
                   Anilist.convertAnimeList(conf.input) match {
                     case Right(list) =>
                       println(s"Found ${list.fullList.length} anime entries for ${list.displayName}")
                       startAnimeFetching(list, conf)
                     case Left(message) =>
                       println(s"Error while parsing JSON file: $message")
                   }
               }

            case MangaFetching =>
              conf.input match {
                case "" =>
                  println(s"Fetching ${conf.username}'s manga list from anilist...")
                  Thread.sleep(1000)
                  println("Actually this isn't doing anything since I haven't implemented this functionality yet.")
                  println("Sorry")
                case _ =>
                  println(s"Parsing ${conf.input}...")
                  Anilist.convertMangaList(conf.input) match {
                    case Right(list) =>
                      println(s"Found ${list.fullList.length} manga entries for ${list.displayName}")
                      startMangaFetching(list, conf)
                    case Left(message) =>
                      println(s"Error while parsing JSON file: $message")
                  }
              }
          }
        }
      case None =>
        println("Error parsing command line args")
    }
  }
}
