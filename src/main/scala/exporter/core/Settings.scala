package exporter.core

import com.typesafe.config.{Config, ConfigFactory}

import scala.util.Try

/**
  * Created by Zoopditto on 20/04/2016.
  */
object Settings {
  sealed trait ErrorResult
  case object NoFile extends ErrorResult
  case class InvalidParameters(params: Set[Parameter]) extends ErrorResult

  abstract class Parameter(implicit val conf: Try[Config]) {
    type ParameterType
    val typeName: String
    val path: String
    def value: Try[ParameterType]
    override def toString: String = s"$path: $typeName"
  }
  class StringParameter(val path: String) extends Parameter {
    type ParameterType = String
    val typeName = "string"
    override def value = conf.map(_.getString(path))
  }
  class OptionalStringParameter(path: String, default: String) extends StringParameter(path) {
    override def value = super.value orElse Try(default)
  }
  class IntParameter(val path: String) extends Parameter {
    type ParameterType = Int
    val typeName = "integer"
    override def value = conf.map(_.getInt(path))
  }
  class OptionalIntParameter(path: String, default: Int) extends IntParameter(path) {
    override def value = super.value orElse Try(default)
  }

  val defaultFilename = "application.conf"
  private implicit lazy val config = Try(ConfigFactory.load(defaultFilename))

  // Private parameters
  private val malUsernameParam = new StringParameter("MAL.username")
  private val malEmailParam = new StringParameter("MAL.email")
  private val malPasswordParam = new StringParameter("MAL.password")
  private val malApiAnimeSearchURLParam = new StringParameter("MAL." +
    "apiAnimeSearchUrl")
  private val malApiMangaSearchURLParam = new StringParameter("MAL." +
    "apiMangaSearchUrl")
  private val fetcherPoolSizeParam =
    new OptionalIntParameter("fetcher.poolSize", 20)
  private val fetcherThrottlingCallsParam
  = new OptionalIntParameter("fetcher.throttlingCalls", 5)
  private val fetcherThrottlingLapseMillisParam
  = new OptionalIntParameter("fetcher.throttlingLapseMillis", 1000)

  private val requiredParams = Set(
    malUsernameParam,
    malEmailParam,
    malPasswordParam,
    malApiAnimeSearchURLParam,
    malApiMangaSearchURLParam
  )

  // Public parameters
  lazy val malUsername: String = malUsernameParam.value.get
  lazy val malEmail: String = malEmailParam.value.get
  lazy val malPassword: String = malPasswordParam.value.get
  lazy val malApiAnimeSearchURL: String = malApiAnimeSearchURLParam.value.get
  lazy val malApiMangaSearchURL: String = malApiMangaSearchURLParam.value.get
  lazy val fetcherPoolSize: Int = fetcherPoolSizeParam.value.get
  lazy val fetcherThrottlingCalls: Int = fetcherThrottlingCallsParam.value.get
  lazy val fetcherThrottlingLapseMillis: Int =
    fetcherThrottlingLapseMillisParam.value.get

  /**
    * Initializes the object with the configuration file.
    * If anything other than None is returned by the function then its parameters
    * should not be called due to risk of them throwing exceptions
    * @return result
    */
  def checkValidity: Option[ErrorResult] = {
    if (config.isFailure)
      return Some(NoFile)

    val invalidParams: Set[Parameter] = requiredParams.filter(_.value.isFailure).toSet
    if (invalidParams.nonEmpty)
      return Some(InvalidParameters(invalidParams))

    None // Everything went daijoubu
  }
}
