package exporter.core

import exporter.MAL.FetcherSupervisor.{AmbiguousEntry, Entry, InvalidEntry, ValidEntry}
import exporter.anilist.MediaInfo
import exporter.anilist.animeTypes.{AnimeInfo, AnimeRecord}
import exporter.anilist.mangaTypes.{MangaInfo, MangaRecord}

import scala.collection.immutable.Queue
import scala.util.Try

/**
  * Created by Zoopditto on 26/04/2016.
  */
object CliUtils {

  private def toCount(n: Option[Int]) = n match {
    case None => "???"
    case Some(0) => "???"
    case Some(c) => c.toString
  }

  def printResults(queue: Queue[Entry]): Unit = {
    val validEntries = queue.filter(_.isValid)
    val invalidEntries = queue.filter(_.isInvalid)
    val ambiguousEntries = queue.filter(_.isAmbiguous)

    println("Results:")
    println(s"\t${validEntries.size} entries were found correctly")
    println(s"\t${invalidEntries.size} entries were not found:")
    invalidEntries.foreach { entry: Entry =>
      println(s"\t\t- ${entry.media.info.titleRomaji}")
    }
    println(s"\t${ambiguousEntries.size} entries had more than one possible " +
      s"result:")
    ambiguousEntries.foreach { entry: Entry =>
      println(s"\t\t- ${entry.media.info.titleRomaji}")
    }
  }

  def eliminateAmbiguity(queue: Queue[AmbiguousEntry]): Queue[ValidEntry] = {
    queue.foldLeft(Queue(): Queue[ValidEntry]) { (validEntries, entry) =>
      println(s"Choose best match for ${entry.media.info.titleRomaji}:")
      entry.media match {
        case anime: AnimeRecord =>
          val opts = entry.options.asInstanceOf[Set[AnimeInfo]]
            .toIndexedSeq
            .sortWith(_.titleRomaji < _.titleRomaji)
          printAnimeOptions(opts)
          getUserOption(opts) match {
            case Some(opt) =>
              validEntries.enqueue(ValidEntry(anime.copy(info =
                opt.asInstanceOf[AnimeInfo])))
            case None => validEntries
          }

        case manga: MangaRecord =>
          val opts = entry.options.asInstanceOf[Set[MangaInfo]]
            .toIndexedSeq
            .sortWith(_.titleRomaji < _.titleRomaji)
          printMangaOptions(opts)
          getUserOption(opts) match {
            case Some(opt) =>
              validEntries.enqueue(ValidEntry(manga.copy(info =
                opt.asInstanceOf[MangaInfo])))
            case None => validEntries
          }
      }
    }
  }

  def printAnimeOptions(opts: IndexedSeq[AnimeInfo]): Unit = {
    opts.foreach { info =>
      val ep = toCount(info.totalEpisodes)
      println(s"(${opts.indexOf(info) + 1}) - ${info.titleRomaji} (${info
        .mediaType}) - ${info.releaseStatus}: $ep episodes")
    }
    println("(0) - Omit entry from the list")
  }

  def printMangaOptions(opts: IndexedSeq[MangaInfo]): Unit = {
    opts.foreach { info =>
      val chap = toCount(info.totalChapters)
      val vol = toCount(info.totalVolumes)
      println(s"(${opts.indexOf(info) + 1}) - ${info.titleRomaji} (${info
        .mediaType}) - ${info.releaseStatus}: $chap chapters, $vol volumes")
    }
    println("(0) - Omit entry from the list")
  }

  def getUserOption(opts: IndexedSeq[MediaInfo]): Option[MediaInfo] = {
    def isInRange(n: Int): Boolean = n >= 0 && n <= opts.size
    var input = -1
    do {
      print(s"Which one (0 - ${opts.size})? ")
      input = Try(Console.in.readLine.replace("\n", "").toInt)
        .getOrElse(-1)
    } while (!isInRange(input))

    input match {
      case 0 => None
      case n => Some(opts(n - 1))
    }
  }
}
