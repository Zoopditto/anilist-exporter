package exporter.anilist

import scala.io.Source
import argonaut._
import Argonaut._
import exporter.anilist.animeTypes.{AnimeList, AnimeRecord}
import exporter.anilist.mangaTypes.{MangaList, MangaRecord}

import scalaz._
import Scalaz._

object Anilist {

  def fetchAnimeList(username: String): List[AnimeRecord] = ??? // TODO

  def fetchMangaList(username: String): List[MangaRecord] = ??? // TODO

  def convertAnimeList(filename: String): Either[String, AnimeList] = {
    import scalaz.{Success, Failure}

    val content = Source.fromFile(filename).getLines.reduce(_ + _)
    Parse.decodeValidation[AnimeList](content) match {
      case Success(animeList) => Right(animeList)
      case Failure(message) => Left(message)
    }
  }

  def convertMangaList(filename: String): Either[String, MangaList] = {
    import scalaz.{Success, Failure}

    val content = Source.fromFile(filename).getLines.reduce(_+_)
    Parse.decodeValidation[MangaList](content) match {
      case Success(mangaList) => Right(mangaList)
      case Failure(message) => Left(message)
    }
  }
}
