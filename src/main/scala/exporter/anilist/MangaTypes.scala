package exporter.anilist

import scalaz._, Scalaz._
import argonaut._, Argonaut._

/**
  * Created by Zoopditto on 23/04/2016.
  */
package object mangaTypes {
  case class MangaList(id: Int,
                       displayName: String,
                       mangaChap: Int,
                       about: String,
                       lists: Map[String, List[MangaRecord]])
    extends MediaList {
    def readingCount: Int = lists("reading").size
    def completedCount: Int = lists("completed").size
    def onHoldCount: Int = lists("on_hold").size
    def droppedCount: Int = lists("dropped").size
    def planCount: Int = lists("plan_to_read").size
  }
  implicit def MangaListCodecJson: CodecJson[MangaList] =
    casecodec5(MangaList.apply, MangaList.unapply)(
      "id",
      "display_name",
      "manga_chap",
      "about",
      "lists")

  case class MangaRecord(recordId: Int,
                         listStatus: String,
                         score: Int,
                         notes: Option[String],
                         reread: Option[Int],
                         scoreRaw: Int,
                         chaptersRead: Option[Int],
                         volumesRead: Option[Int],
                         info: MangaInfo)
    extends MediaRecord
  implicit def MangaRecordCodecJson: CodecJson[MangaRecord] =
    casecodec9(MangaRecord.apply, MangaRecord.unapply)(
      "record_id",
      "list_status",
      "score",
      "notes",
      "reread",
      "score_raw",
      "chapters_read",
      "volumes_read",
      "manga"
    )

  case class MangaInfo(id: Int,
                       titleRomaji: String,
                       mediaType: String,
                       adult: Boolean,
                       releaseStatus: String,
                       totalChapters: Option[Int],
                       totalVolumes: Option[Int])
    extends MediaInfo
  implicit def MangaInfoCodecJson: CodecJson[MangaInfo] =
    casecodec7(MangaInfo.apply, MangaInfo.unapply)(
      "id",
      "title_romaji",
      "type",
      "adult",
      "publishing_status",
      "total_chapters",
      "total_volumes")
}
