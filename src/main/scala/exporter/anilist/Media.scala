package exporter.anilist

/**
  * Created by Zoopditto on 23/04/2016.
  */
sealed trait Media
trait Anime extends Media
trait Manga extends Media

trait MediaList {
  val lists: Map[String, List[MediaRecord]]
  def fullList: List[MediaRecord] = {
    val full = for {
      (_, list) <- this.lists
      record <- list
    } yield record
    full.toList
  }
  def totalCount: Int = fullList.size
}

trait MediaRecord {
  val info: MediaInfo
}

trait MediaInfo {
  val id: Int
  val titleRomaji: String
}

