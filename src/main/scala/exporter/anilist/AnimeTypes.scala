package exporter.anilist

import scalaz._
import Scalaz._
import argonaut._
import Argonaut._

/**
  * Created by Zoopditto on 22/04/2016.
  */
package object animeTypes {
  case class AnimeList(id: Int,
                       displayName: String,
                       animeTime: Int,
                       about: String,
                       lists: Map[String, List[AnimeRecord]])
    extends MediaList {
    def watchingCount: Int = lists("watching").size
    def completedCount: Int = lists("completed").size
    def onHoldCount: Int = lists("on_hold").size
    def planCount: Int = lists("plan_to_watch").size
    def droppedCount: Int = lists("dropped").size
  }
  implicit def AnimeListCodecJson: CodecJson[AnimeList] =
    casecodec5(AnimeList.apply, AnimeList.unapply)(
      "id",
      "display_name",
      "manga_chap",
      "about",
      "lists")

  case class AnimeRecord(recordId: Int,
                         listStatus: String,
                         score: Int,
                         rewatched: Option[Int],
                         notes: Option[String],
                         scoreRaw: Int,
                         episodesWatched: Option[Int],
                         info: AnimeInfo)
    extends MediaRecord
  implicit def AnimeRecordCodecJson: CodecJson[AnimeRecord] =
    casecodec8(AnimeRecord.apply, AnimeRecord.unapply)(
      "record_id",
      "list_status",
      "score",
      "rewatched",
      "notes",
      "score_raw",
      "episodes_watched",
      "anime")

  case class AnimeInfo(id: Int,
                       titleRomaji: String,
                       mediaType: String,
                       adult: Boolean,
                       releaseStatus: String,
                       totalEpisodes: Option[Int])
    extends MediaInfo
  implicit def AnimeInfoCodecJson: CodecJson[AnimeInfo] =
    casecodec6(AnimeInfo.apply, AnimeInfo.unapply)(
      "id",
      "title_romaji",
      "type",
      "adult",
      "airing_status",
      "total_episodes")
}
