val akkaVersion = "2.4.4"

lazy val root = (project in file(".")).
  settings(
    name := "anilist-exporter",
    version := "0.0.1",
    scalaVersion := "2.11.8",

    mainClass := Some("exporter.core.Core"),

    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor" % akkaVersion,
      "com.typesafe.akka" %% "akka-http-core" % akkaVersion,
      "com.typesafe.akka" %% "akka-http-experimental" % akkaVersion,

      "com.typesafe" % "config" % "1.3.0",
      "com.github.scopt" %% "scopt" % "3.4.0",

      "io.argonaut" %% "argonaut" % "6.1"
    )
  )




